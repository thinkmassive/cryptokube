# CryptoKube

Free and open source software stack for peer-to-peer networking on Kubernetes

## UNSTABLE SOFTWARE

This is a pre-release project under active development. The main branch should be relatively stable, but do not run this software unless you have read and understood the code. This notice will be removed when the project reaches alpha quality.

## Installation

### RockPro64

- Install [Armbian Focal current](https://dl.armbian.com/rockpro64/Focal_current) (not xfce version) to a micro-SD card (8gb or more)
  - references: Pine64 [RockPro64](https://wiki.pine64.org/wiki/ROCKPro64), [Armbian docs](https://docs.armbian.com/), [RockPro64 @ Armbian](https://www.armbian.com/rockpro64/)
- Boot from SD, login as root, change password
- Complete the Armbian setup (create normal user, select timezone & locale, etc)
- Make note of the IP
```bash
# on rockpro64
ip addr show eth0
```
- Make *cryptokube* resolvable to the IP of the RockPro64 from your admin host (laptop, etc)
  - Add it to your DNS or /etc/hosts
  - Verify with `ping cryptokube`
- Copy SSH key to rockpro64
```bash
# generate SSH key (skip if you already have one)
ssh-keygen

# copy public key to the normal user account
ssh-copy-id user@cryptokube
```
- Clone this repo and change to the ansible dir
```bash
git clone cryptokube
cd cryptokube/ansible
```
- Run the firstboot playbook:
```bash
ansible-playbook firstboot.yml --ask-become-pass -e initial_run=yes
```
---

## Overview

The goal of this project is to provide an integrated system for running libre P2P software (bitcoin!) using kubernetes on one or more single board computers.

Visit the [Milestones page](https://gitlab.com/thinkmassive/cryptokube/-/milestones) to see the current project status.

### Why kubernetes?

Containers provide a convenient standard for distributing server applications. Sometimes they recieve a bad reputation due to poor implementation, but containers can be locked down as effectively as regular processes. Our container images will be hardened using CIS and NIST guidelines, and regularly scanned for known vulnerabilities.

Kubernetes is a better tool than Docker Compose for running containerized workloads, and allows scaling to multiple hosts more easily than systemd. A few advantages include rolling updates/rollbacks, fine-grained networking and storage, role-based access control, and multi-node support. Helm provides a packaging standard for cluster applications.

Initially the control plane and node components will all run on a single host. Helm charts will be created to deploy tor and a barebones bitcoind+LND stack. When this is running smoothly we'll integrate more ecosystem components and expand to additional worker nodes.

### Initial Components

Here's the plan for initial components:

#### Ansible
  - playbooks to set up SBCs:
    - [Armbian](https://docs.armbian.com/) on [RockPro64](https://wiki.pine64.org/wiki/ROCKPro64)
    - [Raspberry Pi OS Lite](https://www.raspberrypi.org/software/operating-systems/) on [RPi 4b](https://www.raspberrypi.org/products/raspberry-pi-4-model-b/)
  - firewall, ssh, other security
  - docker
  - kubernetes using [kubeadm](https://kubernetes.io/docs/setup/production-environment/tools/kubeadm/), supporting single-node or single-master multi-node, with flannel overlay network
  - persistent local storage

#### Charts & images
  - [tor-controller](https://github.com/kragniz/tor-controller)
  - bitcoind
  - mempool.space
  - LND
  - lndmon
  - LiT
  - ThunderHub

In the future I'd like to add:
  - privacy - dojo, whirlpool, monero
  - bitcoin apps - specter, electrum
  - lightning apps - aperture, faraday, sphinx, lnd hub
  - other p2p - ipfs, sidetree/ion, zeronet
  - observability - prometheus, grafana, ELK
  - supply-chain - gitea, harbor, drone

Much later on I'd like to explore minimizing the OS even more, with something like [SkiffOS](https://github.com/skiffos/SkiffOS) that's based on Buildroot.
